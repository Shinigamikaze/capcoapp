package com.capco.CrudApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableWebMvc
public class CapcoCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(CapcoCrudApplication.class, args);
	}
}
