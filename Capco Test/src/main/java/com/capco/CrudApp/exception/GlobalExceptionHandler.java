package com.capco.CrudApp.exception;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@ControllerAdvice
@Component
public class GlobalExceptionHandler {

	/**
	 * Handles IllegalArgumentException
	 * 
	 * @param IllegalArgumentException
	 *            e
	 * @return the custom messages inserted
	 */
	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public final String exceptionHandlerIllegalArgumentException(final IllegalArgumentException e) {
		return '"' + e.getMessage() + '"';
	}

	/**
	 * Exception handling for invalid date format and null/invalid boolean cases
	 * 
	 * @param MethodArgumentTypeMismatchException
	 *            e
	 * @return the custom message based on either boolean error or invalid Date
	 *         Format that does not follow ISO format: yyyy-MM-dd
	 */
	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public final String exceptionHandlerMethodArgumentTypeMismatchException(
			final MethodArgumentTypeMismatchException e) {
		String returnMessage = "";
		if (StringUtils.containsIgnoreCase(e.getMessage(), "boolean")) {
			returnMessage = "{\"error\":\"Boolean value is incorrect, Please input a valid boolean value.\"}";

		} else {
			returnMessage = "{\"error\":\"Invalid Date Format, Please change your date format to yyyy-MM-dd\"}";

		}
		return returnMessage;
	}

	/**
	 * Handles NullPointerException
	 * 
	 * @param IllegalArgumentException
	 *            e
	 * @return the custom messages inserted
	 */
	@ExceptionHandler(NullPointerException.class)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ResponseBody
	public final String exceptionHandlerNullPointerException(final NullPointerException e) {
		return '"' + e.getMessage() + '"';
	}

}
