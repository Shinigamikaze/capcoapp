package com.capco.CrudApp.Controller;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.springframework.http.HttpStatus.OK;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.capco.CrudApp.db.domain.DbList;
import com.capco.CrudApp.jpa.DbListRepository;

/*
 * Controller class for interfacing
 */
@RestController
@Validated
public class ListController {

	@Autowired
	private DbListRepository dblistRepository;

	/**
	 * Add a List Entry
	 * 
	 * @param name
	 *            (required)
	 * @param dateTime
	 *            (ISO Date format: yyyy-MM-dd)
	 */
	@ResponseStatus(OK)
	@RequestMapping(method = RequestMethod.POST, value = "/add")
	public void add(@RequestParam String name,
			@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @Valid Date dateTime) {
		DbList saveList = new DbList();
		saveList.setName(name);
		saveList.setDate(dateTime);
		dblistRepository.save(saveList);
	}

	/**
	 * Delete a List Entry
	 * 
	 * @param id
	 *            (required)
	 */
	@ResponseStatus(OK)
	@RequestMapping(method = RequestMethod.DELETE, value = "/delete")
	public void delete(@RequestParam Long id) {
		Optional<DbList> dbList = null;
		if (id != null) {
			dbList = dblistRepository.findById(id);
		} else {
			throw new NullPointerException("{\"error\":\"Id is empty, Please input an id\"}");
		}
		if (dbList.isPresent()) {
			DbList returnedDbList = dbList.get();
			dblistRepository.delete(returnedDbList);
		} else {
			throw new IllegalArgumentException(
					"{\"error\":\"Your id is not found, Please find a valid id with list.\"}");
		}
	}

	/**
	 * Update Complete indicator for entry, based on id
	 * 
	 * @param id
	 *            (required)
	 * @param isComplete
	 *            (required)
	 */
	@ResponseStatus(OK)
	@RequestMapping(method = RequestMethod.PUT, value = "/markComplete")
	public void markComplete(@RequestParam Long id, @RequestParam @Valid boolean isComplete) {
		Optional<DbList> dbList = null;
		// Check Id if not null, throws exception if null
		if (id != null) {
			dbList = dblistRepository.findById(id);
		} else {
			throw new NullPointerException("{\"error\":\"Id is empty, Please input an id\"}");
		}

		// Check if entity have been returned, throws exception if not
		if (dbList.isPresent()) {
			DbList returnedDbList = dbList.get();
			returnedDbList.setCompleted(isComplete);
			dblistRepository.save(returnedDbList);
		} else {
			throw new IllegalArgumentException(
					"{\"error\":\"Your id is not found, Please find a valid id with list.\"}");
		}

	}

	/**
	 * Update important indicator for entry, based on id
	 * 
	 * @param id
	 *            (required)
	 * @param isImportant
	 *            (required)
	 */
	@ResponseStatus(OK)
	@RequestMapping(method = RequestMethod.PUT, value = "/markImportant")
	public void markImportant(@RequestParam Long id, @RequestParam boolean isImportant) {
		Optional<DbList> dbList = null;

		// Check Id if not null, throws exception if null
		if (id != null) {
			dbList = dblistRepository.findById(id);
		} else {
			throw new NullPointerException("{\"error\":\"Id is empty, Please input an id\"}");
		}

		// Check if entity have been returned, throws exception if not
		if (dbList.isPresent()) {
			DbList returnedDbList = dbList.get();
			returnedDbList.setImportant(isImportant);
			dblistRepository.save(returnedDbList);
		} else {
			throw new IllegalArgumentException(
					"{\"error\":\"Your id is not found, Please find a valid id with list.\"}");
		}

	}

	/**
	 * Update name or dateTime for entry, based on id
	 * 
	 * @param id
	 *            (required)
	 * @param name
	 * @param dateTime(ISO
	 *            Date format: yyyy-MM-dd)
	 */
	@ResponseStatus(OK)
	@RequestMapping(method = RequestMethod.PUT, value = "/update")
	public void update(@RequestParam Long id, @RequestParam(required = false) String name,
			@RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @Valid Date dateTime) {
		Optional<DbList> dbList = null;

		// Check Id if not null, throws exception if null
		if (id != null) {
			dbList = dblistRepository.findById(id);
		} else {
			throw new NullPointerException("{\"error\":\"Id is empty, Please input an id\"}");
		}

		// Check if entity have been returned, throws exception if not
		if (dbList.isPresent()) {
			DbList returnedDbList = dbList.get();
			// Check if both name and dateTime varibale is not null, throws exception if
			// both null, else check indvidually and then insert and save
			if (isEmpty(name) && dateTime != null) {
				throw new NullPointerException(
						"{\"error\":\"Both name and dateTime are empty, Please input one of the two fields\"}");
			} else {
				if (!isEmpty(name)) {
					returnedDbList.setName(name);
				}
				if (dateTime != null) {
					returnedDbList.setDate(dateTime);
				}
				dblistRepository.save(returnedDbList);
			}
		} else {
			throw new IllegalArgumentException(
					"{\"error\":\"Your id is not found, Please find a valid id with list.\"}");
		}

	}

	/**
	 * Return full list from database
	 * 
	 * @return List<DbList>
	 */
	@ResponseStatus(OK)
	@RequestMapping(method = RequestMethod.GET, value = "/list")
	public List<DbList> list() {
		List<DbList> dbList = dblistRepository.findAll();
		// Check if ReturnAll is not null, throws exception if null
		if (dbList.size() == 0) {
			throw new NullPointerException(
					"{\"error\":\"There are currently nothing in the list database, Please add one.\"}");
		} else {
			return dbList;
		}
	}

}
