package com.capco.CrudApp.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.capco.CrudApp.db.domain.DbList;

@Repository
public interface DbListRepository extends JpaRepository<DbList, Long> {

}
