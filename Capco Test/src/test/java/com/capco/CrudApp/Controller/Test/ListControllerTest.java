package com.capco.CrudApp.Controller.Test;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.capco.CrudApp.Controller.ListController;
import com.capco.CrudApp.db.domain.DbList;

@RunWith(SpringRunner.class)
@WebMvcTest(ListController.class)
public class ListControllerTest {
	@Autowired
	private MockMvc mvc;
	@MockBean
	private ListController listController;

	@Test
	public void testList() throws Exception {
		DbList dbList = new DbList();
		dbList.setId(1L);
		dbList.setDate(Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant()));
		dbList.setImportant(false);
		dbList.setCompleted(false);
		dbList.setName("Testing");
		List<DbList> allDbLists = singletonList(dbList);

		given(listController.list()).willReturn(allDbLists);

		MockHttpServletResponse response = mvc.perform(get("/list").accept(MediaType.APPLICATION_JSON)).andReturn()
				.getResponse();
		assertEquals(response.getStatus(), HttpStatus.OK.value());
	}

}
